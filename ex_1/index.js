const khuX = "X";
const khuA = "A";
const khuB = "B";
const khuC = "C";
const so0 = "0";
const so1 = "1";
const so2 = "2";
const so3 = "3";
function tinhDiemKhuVuc(khuVuc) {
    var diemKhuVuc;
    if (khuVuc == khuA) {
        diemKhuVuc = 2
    } else if (khuVuc == khuB) {
        diemKhuVuc = 1
    } else if (khuVuc == khuC) {
        diemKhuVuc = 0.5
    } else { diemKhuVuc = 0 }
    return diemKhuVuc;
}
function tinhDiemDoiTuong(doiTuong) {
    var diemDoiTuong;
    if (doiTuong == so1) {
        diemDoiTuong = 2.5
    } else if (doiTuong == so2) {
        diemDoiTuong = 1.5
    } else if (doiTuong == so3) {
        diemDoiTuong = 1
    } else {
        diemDoiTuong = 0
    }
    return diemDoiTuong;
}
function ketQua() {
    var diemChuan = document.getElementById("txt-diem-chuan").value * 1;
    var diemToan = document.getElementById("txt-diem-toan").value * 1;
    var diemHoa = document.getElementById("txt-diem-hoa").value * 1;
    var diemLy = document.getElementById("txt-diem-ly").value * 1;
    var loaiKhuVuc = document.getElementById("txt-khu-vuc").value;
    var soDiemKhuVuc = tinhDiemKhuVuc(loaiKhuVuc);
    var loaiDoiTuong = document.getElementById("txt-doi-tuong").value;
    var soDiemDoiTuong = tinhDiemDoiTuong(loaiDoiTuong);
    var tongDiem = diemToan + diemLy + diemHoa;
    if (diemToan == 0 || diemLy == 0 || diemHoa == 0) {
        document.getElementById("result").innerHTML = `<p>Xin chia buon, ban bi diem liet </p>`
    } else if (tongDiem + soDiemKhuVuc + soDiemDoiTuong >= diemChuan){
        document.getElementById("result").innerHTML = `<p>Xin chuc mung, ban da dau </p>`
    } else {document.getElementById("result").innerHTML = ` <p>Xin chia buon, ban da truot</p>`}
}